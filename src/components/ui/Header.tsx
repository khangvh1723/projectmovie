import { NavLink, useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import { Avatar, Popover } from '.'
import { UserOutlined } from '@ant-design/icons'
import { PATH } from 'constant'

export const Header = () => {
    const navigate = useNavigate()
    return <HeaderS>
        <div className='header-content'>
            <h2 className='font-600 text-[30px]'>CYBER MOVIE</h2>
            <div className='flex justify-around'>
                <NavLink className='mr-40' to=''>About</NavLink>
                <NavLink to=''>Contact</NavLink>
                <Popover content={
                    <div className='p-10'>
                        <h2 className='p-10 font-600 mb-10'>User name</h2>
                        <hr />
                        <div className='p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300'
                            onClick={() => {
                                navigate(PATH.account)
                            }}
                        >
                            Thông tin tài khoản
                        </div>
                        <div className='p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300'>
                            Đăng xuất
                        </div>
                    </div>
                }>
                    <Avatar
                        className='!ml-40 !cursor-pointer !flex !items-center !justify-center'
                        size={26}
                        icon={<UserOutlined />}
                    />
                </Popover>
            </div>
        </div>
    </HeaderS>
}

export default Header

const HeaderS = styled.header`
height: var(--header-height);
background: white;
box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
.header-content {
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
}
`