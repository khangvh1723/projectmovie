import { Button as ButtonA, ButtonProps as ButtonPropsA } from 'antd'

type ButtonProps = ButtonPropsA & {
    // defined additionally some props can pass by
}

export const Button = (props: ButtonProps) => {
    return <ButtonA {...props} />
}
export default Button