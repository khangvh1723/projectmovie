import { HTMLInputTypeAttribute } from "react"
import { UseFormRegister } from "react-hook-form"

type InputProps = {
    register?: UseFormRegister<any>,
    error?: string,
    name?: string,
    placeholder?: string,
    type?: HTMLInputTypeAttribute
}

export const Input = ({ register, error, name, placeholder, type }: InputProps) => {
    return (
        <div>
            <input
                type={type}
                placeholder={placeholder}
                className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                {...register(name)}
            />
            {error && <p className="text-red-500">{error}</p>}
        </div>
    )
}

export default Input