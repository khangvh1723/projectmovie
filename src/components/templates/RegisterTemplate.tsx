import { zodResolver } from '@hookform/resolvers/zod';
// import axios from 'axios';
import { toast } from 'react-toastify'
import { useForm, SubmitHandler } from 'react-hook-form'
import { RegisterSchema, RegisterSchemaType } from 'schema';
import { quanLyNguoiDungServices } from 'services';
import { useNavigate } from 'react-router-dom';
import { PATH } from 'constant';
const RegisterTemplate = () => {
       const { handleSubmit,
              register,
              formState: { errors },
       } = useForm<RegisterSchemaType>({
              mode: 'onChange',
              resolver: zodResolver(RegisterSchema),
       })

       const navigate = useNavigate()
       console.log(errors);
       const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
              console.log(value);
              // CORS => thêm https
              // await axios.post('http://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy', value, {
              //        headers: {
              //               TokenCyberSoft:
              //                      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjIyLzAxLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwNTg4MTYwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2MDI5MjAwfQ.7_G72JssvlfZA0SzyXUjBEuFceGkXY70Ar4ixqy-Wh0',
              //        },
              // })
              try {
                     await quanLyNguoiDungServices.register(value)
                     toast.success('Đăng ký thành công')
                     navigate(PATH.login)
              } catch (error) {
                     toast.error(error?.response?.data?.content)
              }
       }
       return (
              <div>
                     <form className="px-[30px]" onSubmit={handleSubmit(onSubmit)}>
                            <h2 className="text-white text-40 font-600">Register</h2>
                            <div className="mt-20">
                                   <input
                                          type="text"
                                          id="large-input"
                                          placeholder="Tài khoản"
                                          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                                          // {...register('taiKhoan', {
                                          // required: 'Vui lòng nhập tài khoản',
                                          // minLength: {
                                          //        value: 6,
                                          //        message: 'Nhập từ 6 ký tự',
                                          // },
                                          // maxLength: {
                                          //        value: 20,
                                          //        message: 'Nhập tối đa 20 ký tự'
                                          // },
                                          // pattern: {
                                          //        value: ,
                                          //        message: '',
                                          // }
                                          // })}
                                          {...register('taiKhoan')}
                                   />
                                   {/* <p className='text-red-500'>{errors?.taiKhoan?.message as string}</p> */}
                                   <p className='text-red-500'>{errors?.taiKhoan?.message}</p>
                            </div>
                            <div className="mt-20">
                                   <input
                                          type="password"
                                          placeholder="Mật khẩu"
                                          id="large-input"
                                          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                                          {...register('matKhau')}
                                   />
                                   <p className='text-red-500'>{errors?.matKhau?.message}</p>
                            </div>
                            <div className="mt-20">
                                   <input
                                          type="email"
                                          placeholder="Email"
                                          id="large-input"
                                          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                                          {...register('email')}
                                   />
                                   <p className='text-red-500'>{errors?.email?.message}</p>
                            </div>
                            <div className="mt-20">
                                   <input
                                          type="text"
                                          placeholder="Số điện thoại"
                                          id="large-input"
                                          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                                          {...register('soDt')}
                                   />
                                   <p className='text-red-500'>{errors?.soDt?.message}</p>
                            </div>
                            <div className="mt-20">
                                   <input
                                          type="text"
                                          placeholder="Mã nhóm"
                                          id="large-input"
                                          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                                          {...register('maNhom')}
                                   />
                                   <p className='text-red-500'>{errors?.maNhom?.message}</p>
                            </div>
                            <div className="mt-20">
                                   <input
                                          type="text"
                                          placeholder="Họ tên"
                                          id="large-input"
                                          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500"
                                          {...register('hoTen')}
                                   />
                                   <p className='text-red-500'>{errors?.hoTen?.message}</p>
                            </div>
                            <div className="mt-20">
                                   <button
                                          className="w-full text-white bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">Register</button>
                            </div>
                     </form>
              </div>
       )
}

export default RegisterTemplate