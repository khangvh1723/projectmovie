import { useNavigate } from 'react-router-dom'
import { PATH } from '../../constant/config'
import { SubmitHandler, useForm } from 'react-hook-form'
import { LoginSchema, LoginSchemaType } from 'schema/LoginSchema'
import { zodResolver } from '@hookform/resolvers/zod'
// import { quanLyNguoiDungServices } from 'services'
import { toast } from 'react-toastify'
import { useAppDispatch } from 'store'
import { loginThunk } from 'store/quanLyNguoiDung/thunk'
import { Input } from 'components/ui'
const LoginTemplate = () => {

    const navigate = useNavigate()

    const {
        handleSubmit,
        register,
        formState: { errors },
    } = useForm<LoginSchemaType>({
        mode: 'onChange',
        resolver: zodResolver(LoginSchema),
    })

    const dispatch = useAppDispatch()

    const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
        // try {
        //     await quanLyNguoiDungServices.login(value)
        //     toast.success('Đăng nhập thành công')
        //     navigate('/')
        // } catch (error) {
        //     toast.error(error?.response?.data?.content)
        // }

        dispatch(loginThunk(value))
            .unwrap()
            .then(() => {
                toast.success('Đăng nhập thành công')
                navigate('/')
            })
    }
    return (
        <div>
            <form action="" onSubmit={handleSubmit(onSubmit)}>
                <h2 className="text-white text-40 font-600">Sign In</h2>
                <div className="mt-40">
                    {/* <input
                        {...register('taiKhoan')}
                        type="text"
                        id="large-input"
                        placeholder="User name"
                        className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500" />
                    <p className='text-red-500'>{errors?.taiKhoan?.message}</p> */}
                    <Input
                        register={register}
                        name="taiKhoan"
                        error={errors?.taiKhoan?.message}
                        placeholder='Tài khoản'
                        type='text'
                    />
                </div>
                <div className="mt-40">
                    {/* <input
                        {...register('matKhau')}
                        type="password"
                        placeholder="Password"
                        id="large-input"
                        className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] sm:text-md focus:ring-blue-500 focus:border-blue-500" />
                    <p className='text-red-500'>{errors?.matKhau?.message}</p> */}
                    <Input
                        register={register}
                        name="taiKhoan"
                        error={errors?.taiKhoan?.message}
                        placeholder='Mật khẩu'
                        type='password'
                    />
                </div>
                <div className="mt-40">
                    <button
                        className="w-full text-white bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">Sign In</button>
                    <p className="mt-10 text-white">
                        Chưa có tài khoản?{' '}
                        <span
                            className="text-blue-500 cursor-pointer"
                            onClick={() => {
                                navigate(PATH.register)
                            }}
                        >Đăng ký</span>
                    </p>
                </div>
            </form>
        </div>
    )
}

export default LoginTemplate