import { apiInstance } from "constant";
import { RegisterSchemaType } from "schema";
import { LoginSchemaType } from "schema/LoginSchema";
import { User } from "types";

const api = apiInstance(
    {
        baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API,
    }
)
export const quanLyNguoiDungServices = {
    register: (payload: RegisterSchemaType) => api.post('/DangKy', payload),
    login: (payload: LoginSchemaType) => api.post<APIResponse<User>>('/DangNhap', payload),
}