import { RouteObject } from 'react-router-dom'
import { Demo } from 'demo'
import { PATH } from 'constant'
import { AuthLayout, MainLayout } from 'components/layouts'
import { Home, Login, Register } from 'pages'
export const router: RouteObject[] = [
    {
        path: '/demo',
        element: <Demo />,
    },
    {
        path: '/',
        element: <MainLayout />,
        children: [
            {
                index: true,
                element: <Home />,
            }
        ]
    },
    {
        element: <AuthLayout />,
        children: [
            {
                path: PATH.login,
                element: <Login />
            },
            {
                path: PATH.register,
                element: <Register />
            },
        ]
    },
]