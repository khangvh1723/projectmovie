import { createSlice } from '@reduxjs/toolkit'
import { loginThunk } from './thunk'
import { User } from 'types'
type QuanLyNguoiDungInitialState = {
    user?: User
}

const initialState: QuanLyNguoiDungInitialState = {
    user: JSON.parse(localStorage.getItem('USER')),
}
const quanLyNguoiDungSlice = createSlice({
    name: 'quanLyNguoiDung',
    initialState,
    reducers: {}, //xử lý action đổng bộ
    extraReducers: (builder) => {
        // xử lý action bất đồng bộ (call API)
        builder
            // .addCase(loginThunk.pending, (state, { payload }) => {
            //     console.log('payload pending: ', payload);
            // })
            .addCase(loginThunk.fulfilled, (state, { payload }) => {
                console.log('payload fulfilled: ', payload);
                state.user = payload;
                // lưu thông tin đăng nhập vào localstorage
                if (payload) {
                    localStorage.setItem('USER', JSON.stringify(payload))
                }
            })
        // .addCase(loginThunk.rejected, (state, { payload }) => {
        //     console.log('payload rejected: ', payload);
        // })
    }
})

export const { reducer: quanLyNguoiDungReducer, actions: quanLyNguoiDungActions } = quanLyNguoiDungSlice