import { useSelector } from "react-redux"
import { RootState } from "store"

// Lấy thông tin user đăng nhập
// custom hook
export const useAuth = () => {
    const { user } = useSelector((state: RootState) => state.quanLyNguoiDung)
    return { user }
}