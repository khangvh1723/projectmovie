export const PATH = {
    demo: '/demo',
    login: '/login',
    register: '/register',
    account: '/account',
}