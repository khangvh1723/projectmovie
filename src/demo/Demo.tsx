
export const Demo = () => {
  return (
    <div>
        <h2>Demo Tailwind</h2>
        <p className="!text-20 text-[#6868] -ml-10 p-10 hover:text-red-500 py-[20px] transition-all duration-300">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos, sed?
        </p>
    </div>
  )
}

export default Demo